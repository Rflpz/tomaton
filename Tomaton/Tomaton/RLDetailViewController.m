//
//  RLDetailViewController.m
//  Tomaton
//
//  Created by ios on 20/02/14.
//  Copyright (c) 2014 Rafael_Lopez. All rights reserved.
//

#import "RLDetailViewController.h"
#import "UIImageView+AFNetworking.h"


@interface RLDetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *year;
@property (weak, nonatomic) IBOutlet UILabel *rating;
@property (weak, nonatomic) IBOutlet UITextView *synopsis;
@property (weak, nonatomic) IBOutlet UIImageView *background;
@property (weak, nonatomic) UIRefreshControl *refreshControl;



@end

@implementation RLDetailViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Detail movie";
    self.view.window.tintColor = [UIColor colorWithRed:0/256.0 green:191/256.0 blue:255/256.0 alpha:1];

}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self customizeViewInterface];
    [self.refreshControl beginRefreshing];
    [self refreshInfo:self.refreshControl];
    
}

- (void)refreshInfo:(UIView *) sender {
    NSURLSession *sesion = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [sesion dataTaskWithURL:[NSURL URLWithString:self.backURL] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"%@",json);
        if (!error){
            [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                self.name.text = [json valueForKeyPath:@"title"];
                self.year.text = [[json valueForKeyPath:@"year"] stringValue];
                self.synopsis.text = [json valueForKeyPath:@"synopsis"];
                self.rating.text = [json valueForKeyPath:@"ratings.audience_rating"];
                NSString *img = [json valueForKeyPath:@"posters.detailed"];
                NSLog(@"Imagen url : %@", img);
                [self.background setImageWithURL:[NSURL URLWithString:[json valueForKeyPath:@"posters.detailed"]] placeholderImage:nil];
                self.background.alpha =.35;
                self.synopsis.alpha = .9;
                self.name.shadowColor =[UIColor whiteColor];
                self.year.shadowColor = [UIColor whiteColor];
                [UIView animateWithDuration:2
                                 animations:^{
                                     self.name.alpha = 1;
                                     self.year.alpha = 1;
                                     self.synopsis.alpha = 1;
                                     self.rating.alpha = 1;
                                     [self.view layoutIfNeeded];
                                 }];
            }];
            
        }
        else{
            NSLog(@"Hubo un error %@", [error localizedDescription]);
            [[NSOperationQueue mainQueue] addOperationWithBlock:^(void)
             {
                 UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Verifica tu conexion a internet" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 [alert show];
             }];
        }
        
    }];
    [dataTask resume];

}


- (void)customizeViewInterface {
    
    UIRefreshControl *refreshControl= [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self
                       action:@selector(refreshInfo:)
             forControlEvents:UIControlEventValueChanged];
    self.refreshControl =refreshControl;
    
}

@end
