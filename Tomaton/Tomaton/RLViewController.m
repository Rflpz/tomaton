//
//  RLViewController.m
//  Tomaton
//
//  Created by ios on 18/02/14.
//  Copyright (c) 2014 Rafael_Lopez. All rights reserved.
//

#import "RLViewController.h"
#import "RLDetailViewController.h"
#import "UIImageView+AFNetworking.h"
static NSString *URList = @"http://api.rottentomatoes.com/api/public/v1.0/lists/movies/in_theaters.json?page_limit=50&page=1&country=us&apikey=gtsu4v7evh3s52n8xb8ut4ge";
static NSString *APIKey = @"?apikey=gtsu4v7evh3s52n8xb8ut4ge";
NSString *bandera = @"false";
@interface RLViewController ()<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) NSIndexPath *lastSelectedIndexPath;
@property (strong, nonatomic) NSArray *arrayData;
@property (nonatomic, retain) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) NSString *query;

@end

@implementation RLViewController


- (void)viewDidLoad{
    [super viewDidLoad];
    [self customizeViewInterface];
}

- (void)customizeViewInterface {
    
    UIRefreshControl *refreshControl= [[UIRefreshControl alloc]init];
    self.refreshControl.tintColor = [UIColor colorWithRed:0/256.0 green:163/256.0 blue:136/256.0 alpha:1];
    [refreshControl addTarget:self
                       action:@selector(refreshInfo:)
             forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    self.refreshControl =refreshControl;

    
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView deselectRowAtIndexPath:self.lastSelectedIndexPath animated:YES];
    
     self.title = @"Tomaton";
    [self.refreshControl beginRefreshing];
    [self refreshInfo:self.refreshControl];
    
}

- (void)refreshInfo:(UIRefreshControl *) sender {
    NSURLSession *session =[NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:URList]
                                            completionHandler:^(
                                                                NSData *data,
                                                                NSURLResponse *response,
                                                                NSError *error) {
                                                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                NSArray *movies =  [json valueForKeyPath:@"movies"];
                                                NSSortDescriptor *arrayTommatometer = [[NSSortDescriptor alloc] initWithKey:@"ratings.critics_score" ascending:NO];
                                                NSArray *arraycriticScore = [NSArray arrayWithObject:arrayTommatometer];
                                                if (!error) {
                                                    self.arrayData = [movies sortedArrayUsingDescriptors:arraycriticScore];
                                                    [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                                                        [self.tableView reloadData];
                                                        [sender endRefreshing];
                                                    }];
                                                    NSLog(@"%@", self.arrayData);
                                                }
                                                else {
                                                    [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                                                        [sender endRefreshing];
                                                        UIAlertView *help = [[UIAlertView alloc] initWithTitle:@"Help me!"
                                                                                                       message:@"Error"
                                                                                                      delegate:nil
                                                                                             cancelButtonTitle:@"Ok"
                                                                                             otherButtonTitles: nil];
                                                        [help show];
                                                    }];
                                                }
                                            }];
    [dataTask resume];
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arrayData.count;
}

-(UITableViewCell *)tableView:(UITableViewCell *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"cellId";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellId];
    }
    
    NSDictionary *dataInfo = self.arrayData [indexPath.row];
    __weak UITableViewCell *cellImage = cell;
    [cell.imageView setImageWithURLRequest:[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[dataInfo valueForKeyPath:@"posters.thumbnail"]]]
                          placeholderImage:[UIImage imageNamed:nil]
                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image){
                                       cellImage.imageView.image = image;
                                       if(cellImage.imageView.frame.size.height==0 || cellImage.imageView.frame.size.width==0 ){
                                       [cellImage setNeedsLayout];
                                       }
                                   }
                                   failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                   }];
    cell.textLabel.text = dataInfo [@"title"];
    NSString *criticsRating = [dataInfo valueForKeyPath:@"ratings.critics_rating"];
    NSString *tomatometer;
    NSString *detailsMovie;
    if(criticsRating == nil){
        criticsRating = @"";
        tomatometer = [[dataInfo valueForKeyPath:@"ratings.critics_score"] stringValue];
        detailsMovie = [NSString stringWithFormat:@"%@%@%@", @" ",@"Rating: ", tomatometer];
    }
    else
    {
        tomatometer = [[dataInfo valueForKeyPath:@"ratings.critics_score"] stringValue];
        detailsMovie = [NSString stringWithFormat:@"%@%@%@%@", @" ", criticsRating,@" - Rating: ", tomatometer];
        
    }
    cell.detailTextLabel.text = detailsMovie;
    tableView.backgroundColor = [UIColor colorWithRed:67/256.0 green:0/256.0 blue:11/256.0 alpha:.80];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.detailTextLabel.textColor = [UIColor grayColor];
    cell.backgroundColor  = [UIColor colorWithRed:67/256.0 green:0/256.0 blue:11/256.0 alpha:.80];

    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    self.lastSelectedIndexPath = indexPath;
    
    NSDictionary *movieSelected = self.arrayData [indexPath.row];
    NSString *movSelect = [movieSelected valueForKeyPath:@"links.self"];
    NSString *movieInfoTotalSelected = [NSString stringWithFormat:@"%@%@", movSelect, APIKey];
    RLDetailViewController *secondController = [[RLDetailViewController alloc]initWithNibName:@"RLDetailViewController" bundle:nil];
    secondController.backURL = movieInfoTotalSelected;
    [self.navigationController pushViewController:secondController animated:YES];

}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    self.query = self.searchBar.text;
    self.query = [self.query stringByReplacingOccurrencesOfString:@" " withString:@"&"];
    NSLog(@"El resultado es: %@",self.query);
    NSString *busqueda = @"http://api.rottentomatoes.com/api/public/v1.0/movies.json?apikey=gtsu4v7evh3s52n8xb8ut4ge&q=";
    URList = [NSString stringWithFormat:@"%@%@",busqueda, self.query];
    NSLog(@"url formada : %@", URList);
    
    [self.tableView deselectRowAtIndexPath:self.lastSelectedIndexPath animated:YES];
    self.title = @"Tomaton";
    [self.refreshControl beginRefreshing];
    [self refreshInfo:self.refreshControl];
    
    
    
    [searchBar setShowsCancelButton:NO animated:YES];
    searchBar.text=@"";
    [searchBar resignFirstResponder];
    self.tableView.allowsSelection = YES;
    self.tableView.scrollEnabled = YES;
    
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    self.searchBar.text=@"";
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    self.tableView.allowsSelection = YES;
    self.tableView.scrollEnabled = YES;
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
    self.tableView.allowsSelection = NO;
    self.tableView.scrollEnabled = NO;
}
@end
