//
//  main.m
//  Tomaton
//
//  Created by ios on 18/02/14.
//  Copyright (c) 2014 Rafael_Lopez. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RLAppDelegate class]));
    }
}
