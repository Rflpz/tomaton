//
//  RLAppDelegate.h
//  Tomaton
//
//  Created by ios on 18/02/14.
//  Copyright (c) 2014 Rafael_Lopez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
